#include "stdafx.h"
#include "Registers.h"
#include "Convertor.h"

Registers::Registers(HANDLE hProcess,uint32_t imagebase):
	hProcess(hProcess)
{
	addresses = offsets;
	for (auto it = addresses.begin(); it != addresses.end(); ++it)
	{
		*it += imagebase;
	}
}

std::string Registers::PrintRegisters() const
{
	unsigned int number = 0;
	std::string result;
	SIZE_T stub = 0;

	for (const auto& address : addresses)
	{
		uint32_t value;
		ReadProcessMemory(hProcess, reinterpret_cast<LPCVOID> (address), &value, sizeof(value), &stub);
		result += "reg" + std::to_string(number++) + " [ " + Convertor::to_hex(address) + "]: "
			+ Convertor::to_hex(value) + "\n";
	}

	return result;
}