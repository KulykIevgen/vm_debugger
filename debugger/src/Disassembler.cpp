#include "stdafx.h"
#include "Disassembler.h"
#include <stdint.h>

std::string Disassembler::GetCode(uint8_t opcode)
{
	std::string result;
	std::string tmp;
	uint8_t highpart = opcode & 0xF0;
	uint8_t lowpart = opcode & 0x0F;

	switch (highpart)
	{
	case 0:
		switch (lowpart)
		{
		case 3:
			result += "reg0 ^= reg3";
			break;
		default:
			break;
		}
		break;
	case 0x10:
		tmp = "[reg4 + 1]";
		switch (lowpart)
		{
		case 0:
			result += "reg4 = " + tmp + " + reg5\n";
			break;
		default:
			break;
		}
		result += "reg4 += 4";
		break;
	case 0x20:
		tmp = "[reg4 + 1]";
		result += "if reg8 & 1 == 1\n";
		result += "reg4 = reg5 + " + tmp + "\n";
		result += "else\n";
		result += "++reg4\n";
		result += "reg4 += 4";
		break;
	case 0x30:
		tmp = "reg6 + reg7";

		switch (lowpart)
		{
		case 0:
			result = "[" + tmp + " - 4] = reg0\n";
			result += "reg6 -= 4";
			break;
		case 9:
			result = "reg1 = [" + tmp + "]\n";
			result += "reg6 += 4";
			break;
		case 0xB:
			result = "reg3 = [" + tmp + "]\n";
			result += "reg6 += 4";
			break;
		default:
			break;
		}

		break;
	case 0x40:
		switch (lowpart)
		{
		case 0:
			tmp = "[reg4 + 1]";
			result += "reg0 &= " + tmp + "\n";
			result += "reg4 += 4";
			break;
		case 0x2:
			result += "reg0 = [reg0]";
			break;
		case 0x3:
			result = "exit";
			break;
		default:
			break;
		}
		break;
	case 0x60:
		switch (lowpart)
		{
		case 3:
			result += "reg0 += reg3";
			break;
		default:
			break;
		}
		break;
	case 0xB0:
		switch (lowpart)
		{
		case 0:
			result = "reg0 = reg1";
			break;
		}
		break;
	case 0xC0:
		tmp = "[reg4 + 1]";
		switch (lowpart)
		{
		case 0xA:
			result += "reg2 = " + tmp + "\n";
			break;
		case 0xB:
			result += "reg3 = " + tmp + "\n";			
			break;
		default:
			break;
		}
		result += "reg4 += 4";
		break;
	case 0xD0:
		tmp = "[reg4 + 1]";
		switch (lowpart)
		{
		case 8:
			result += "if " + tmp + " == reg0\n";
			break;
		}		
		result += "reg8 |= 1\n";
		result += "else if reg8 == 1\n";
		result += "reg8 &= 2\n";
		result += "else\n";
		result += "reg8 |= 2\n";
		result += "reg4 += 4";
		break;
	case 0xE0:
		switch (lowpart)
		{
		case 9:
			result += "++reg1\n";
			break;
		default:
			break;
		}
		result += "++reg4";
		break;
	default:
		break;
	}

	return result;
}
