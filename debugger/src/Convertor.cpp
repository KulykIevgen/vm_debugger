#include "stdafx.h"
#include "Convertor.h"

std::string Convertor::to_hex(uint32_t val)
{	
	std::string result;
	std::stringstream ss;
	ss << std::hex << val;
	ss >> result;
	return result;
}
