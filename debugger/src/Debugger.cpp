#include "stdafx.h"
#include "Debugger.h"
#include "ErrorInfo.h"
#include "Convertor.h"
#include "Disassembler.h"
#include <iostream>

Debugger::Debugger(const std::string& programPath)
{
	memset(&startupinfo, 0, sizeof(startupinfo));
	memset(&processinfo, 0, sizeof(processinfo));

	if (!CreateProcessA(programPath.c_str(), nullptr, nullptr, nullptr, FALSE,
		DEBUG_PROCESS | DEBUG_ONLY_THIS_PROCESS, nullptr, nullptr, &startupinfo, &processinfo))
	{
		throw ErrorInfo::MakeLastError();
	}

	initializer.reset(new Initializer());	
}

Debugger::~Debugger()
{
	TerminateProcess(processinfo.hProcess, 0);
	CloseHandle(processinfo.hThread);
	CloseHandle(processinfo.hProcess);
}

void Debugger::Init()
{
	uint32_t imagebase = initializer->GetImageBase();
	registers.reset(new Registers(processinfo.hProcess, imagebase));
	uint32_t imagesize = GetImageSize(imagebase);
	vmpoint = FindVmByPattern(imagebase, imagesize);	

	if (!HookVM(vmpoint + offset_to_vm_instruction_processing))
	{
		throw std::logic_error("Unable to hook vm interpreter");
	}
}

void Debugger::single_step()
{
	DEBUG_EVENT debugEvent = { 0 };
	CONTEXT context = { 0 };
	context.ContextFlags = CONTEXT_FULL;
	static bool unhandledStep = false;
	static DWORD ProcessId;
	static DWORD ThreadId;

	if (unhandledStep)
	{
		ContinueDebugEvent(ProcessId, ThreadId, DBG_CONTINUE);
		unhandledStep = false;
	}

	while (WaitForDebugEvent(&debugEvent, INFINITE))
	{
		switch (debugEvent.dwDebugEventCode)
		{
		case EXCEPTION_DEBUG_EVENT:
			switch (debugEvent.u.Exception.ExceptionRecord.ExceptionCode)
			{
			case EXCEPTION_BREAKPOINT:
				if (GetThreadContext(processinfo.hThread, &context))
				{
					if (context.Eip == vmpoint + offset_to_vm_instruction_processing + 1)
					{
						context.Eip += 2;
						uint8_t dl = static_cast<uint8_t> (context.Edx);
						std::cout << "instruction opcode: " << Convertor::to_hex(dl) << std::endl;
						std::cout << Disassembler::GetCode(dl) << std::endl;
						dl &= 0xF;
						context.Edx &= 0xFFFFFF00;
						context.Edx |= dl;
						SetThreadContext(processinfo.hThread, &context);	
						ProcessId = debugEvent.dwProcessId;
						ThreadId = debugEvent.dwThreadId;
						unhandledStep = true;
						//ContinueDebugEvent(debugEvent.dwProcessId, debugEvent.dwThreadId, DBG_CONTINUE);
						return;
					}
					else
					{
						ContinueDebugEvent(debugEvent.dwProcessId, debugEvent.dwThreadId, DBG_EXCEPTION_NOT_HANDLED);
					}
				}
				break;
			default:
				break;
			}
			break;		
		default:
			break;
		}

		ContinueDebugEvent(debugEvent.dwProcessId, debugEvent.dwThreadId, DBG_CONTINUE);
	}
}

std::string Debugger::PrintRegisters() const
{
	return registers->PrintRegisters();
}

void Debugger::DumpMemory(uint32_t address)
{
	const uint32_t size = 16;
	uint8_t buffer[size] = { 0 };
	SIZE_T stub = 0;
	std::string out;

	if (ReadProcessMemory(processinfo.hProcess, reinterpret_cast<LPVOID> (address), buffer, sizeof(buffer), &stub))
	{
		for (auto val : buffer)
		{
			std::cout << Convertor::to_hex(val) << " ";
			out += static_cast<char> (val);
		}
		std::cout << std::endl;
		std::cout << out << std::endl;
	}
}

void Debugger::CommandHandler()
{
	std::cin.unsetf(std::ios::dec);
	std::cin.unsetf(std::ios::hex);
	std::cin.unsetf(std::ios::oct);

	while (true)
	{
		std::string command;
		std::cin >> command;

		if (command == "s")
		{
			this->single_step();
		}
		else if (command == "r")
		{
			std::cout << this->PrintRegisters() << std::endl;
		}
		else if (command == "d")
		{
			uint32_t address;
			std::cin >> address;

			this->DumpMemory(address);
		}
	}
}

uint32_t Debugger::GetImageSize(uint32_t imageBase)
{
	MEMORY_BASIC_INFORMATION basicInfo = { 0 };
	uint32_t result = 0;
	LPCVOID address = reinterpret_cast<LPCVOID>(imageBase);

	do 
	{
		if (!VirtualQueryEx(processinfo.hProcess, address, &basicInfo, sizeof(basicInfo)))
			break;
		if (basicInfo.Type != MEM_IMAGE)
			break;
		result += static_cast<uint32_t> (basicInfo.RegionSize);
		address = reinterpret_cast<LPCVOID> (reinterpret_cast<uint32_t> (address) + basicInfo.RegionSize);
	} while (true);

	return result;
}

uint32_t Debugger::FindVmByPattern(uint32_t imageBase, uint32_t imageSize)
{
	SIZE_T stub = 0;
	std::unique_ptr<uint8_t> data(new uint8_t[imageSize]);
	const uint8_t pattern[] = { 0x8B,
		0x1D, 0x10, 0x30, 0x40, 0x00, 0x33, 0xC0, 0x33, 0xC9, 0x33, 0xD2, 0x8A, 0x03, 0x8A, 0xD0, 0x80,
		0xE2, 0x0F, 0x24, 0xF0 };
	uint32_t result = 0;

	if (ReadProcessMemory(processinfo.hProcess, reinterpret_cast<LPCVOID> (imageBase), data.get(), imageSize, &stub))
	{
		for (unsigned int i = 0; i < imageSize; i++)
		{
			if (!memcmp(&data.get()[i], pattern, sizeof(pattern)))
			{
				result = imageBase + i;
				break;
			}
		}
	}

	return result;
}

bool Debugger::HookVM(uint32_t location)
{
	const uint8_t Breakpoint = 0xCC;
	DWORD oldprotect;
	uint8_t oldByte;

	if (VirtualProtectEx(processinfo.hProcess, reinterpret_cast<LPVOID> (location), 
		sizeof(Breakpoint), PAGE_EXECUTE_READWRITE, &oldprotect))
	{
		SIZE_T stub;
		if (ReadProcessMemory(processinfo.hProcess, reinterpret_cast<LPVOID> (location), &oldByte, sizeof(Breakpoint), &stub))
		{
			if (WriteProcessMemory(processinfo.hProcess, reinterpret_cast<LPVOID> (location), &Breakpoint, sizeof(Breakpoint), &stub))
			{
				originalVmByte = oldByte;
				FlushInstructionCache(processinfo.hProcess, reinterpret_cast<LPVOID> (location), sizeof(Breakpoint));
				VirtualProtectEx(processinfo.hProcess, reinterpret_cast<LPVOID> (location), sizeof(Breakpoint), oldprotect, &oldprotect);
				return true;
			}
		}
	}

	return false;
}
