#include "stdafx.h"
#include "Debugger.h"
#include <iostream>

int main()
{
	Debugger debugger("miniVMcrackme.exe");
	debugger.Init();
	debugger.CommandHandler();
    return 0;
}

