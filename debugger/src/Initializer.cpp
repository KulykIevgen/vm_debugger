#include <thread>

#include "stdafx.h"
#include "Initializer.h"
#include "ErrorInfo.h"

Initializer::Initializer()
{
	imagebase = this->DebugLoop();
}

uint32_t Initializer::DebugLoop()
{
	memset(&debugEvent, 0, sizeof(debugEvent));
	bool loaderBreakpoint = false;
	uint32_t imagebase = 0;

	while (WaitForDebugEvent(&debugEvent, INFINITE))
	{	
		if (imagebase && loaderBreakpoint)
		{
			ContinueDebugEvent(debugEvent.dwProcessId, debugEvent.dwThreadId, DBG_CONTINUE);
			return imagebase;
		}

		switch (debugEvent.dwDebugEventCode)
		{
		case EXCEPTION_DEBUG_EVENT:
			switch (debugEvent.u.Exception.ExceptionRecord.ExceptionCode)
			{
			case EXCEPTION_BREAKPOINT:
				loaderBreakpoint = true;
				break;
			default:
				break;
			}
			break;
		case CREATE_PROCESS_DEBUG_EVENT:
			imagebase = reinterpret_cast<uint32_t> (debugEvent.u.CreateProcessInfo.lpBaseOfImage);
			break;
		default:
			break;
		}

		ContinueDebugEvent(debugEvent.dwProcessId, debugEvent.dwThreadId, DBG_CONTINUE);
	}

	throw ErrorInfo::MakeLastError();
}
