#pragma once
#include <stdint.h>
#include <string>
#include <sstream>

class Convertor
{
public:
	Convertor() = delete;
	~Convertor() = delete;
	Convertor(const Convertor&) = delete;
	Convertor(Convertor&&) = delete;
	Convertor& operator=(const Convertor&) = delete;
	Convertor& operator=(Convertor&&) = delete;

	static std::string to_hex(uint32_t val);
};

