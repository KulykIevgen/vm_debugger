#pragma once
#include <Windows.h>
#include <string>
#include "Initializer.h"
#include "Registers.h"

class Debugger
{
public:
	Debugger() = delete;	
	Debugger(const Debugger&) = delete;
	Debugger(Debugger&&) = delete;
	Debugger& operator=(const Debugger&) = delete;
	Debugger& operator=(Debugger&&) = delete;

	explicit Debugger(const std::string& programPath);
	~Debugger();
	void Init();
	void single_step();
	std::string PrintRegisters() const;
	void DumpMemory(uint32_t address);
	void CommandHandler();

private:
	STARTUPINFOA startupinfo;
	PROCESS_INFORMATION processinfo;	
	std::unique_ptr<Initializer> initializer;
	std::unique_ptr<Registers> registers;
	uint8_t originalVmByte;

	uint32_t vmpoint;
	const uint32_t offset_to_vm_instruction_processing = 16;

	uint32_t GetImageSize(uint32_t imageBase);
	uint32_t FindVmByPattern(uint32_t imageBase, uint32_t imageSize);
	bool HookVM(uint32_t location);
};

