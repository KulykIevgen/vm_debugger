#pragma once
#include <Windows.h>
#include <mutex>
#include <stdint.h>

class Initializer
{
public:	
	Initializer(const Initializer&) = delete;
	Initializer(Initializer&&) = delete;
	Initializer& operator=(const Initializer&) = delete;
	Initializer& operator=(Initializer&&) = delete;

	Initializer();
	~Initializer() = default;
	uint32_t GetImageBase()
	{
		return imagebase;
	}

private:
	DEBUG_EVENT debugEvent;		
	uint32_t DebugLoop();
	uint32_t imagebase;
};

