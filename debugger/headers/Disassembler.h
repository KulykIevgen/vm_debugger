#pragma once
#include <string>

class Disassembler
{
public:
	Disassembler() = delete;
	~Disassembler() = delete;

	static std::string GetCode(uint8_t opcode);
};

