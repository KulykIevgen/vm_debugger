#pragma once
#include <vector>
#include <stdint.h>
#include <map>
#include <string>
#include <Windows.h>

class Registers
{
public:
	Registers() = delete;
	~Registers() = default;
	explicit Registers(HANDLE hProcess,uint32_t imagebase);
	std::string PrintRegisters() const;

private:
	const std::vector<uint32_t> offsets{ 0x3000,0x3004,0x3008,0x300C,0x3010,
								0x3014,0x3018,0x301C,0x3020 };
	std::vector<uint32_t> addresses;	
	HANDLE hProcess;	
};

