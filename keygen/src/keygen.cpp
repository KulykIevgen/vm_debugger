#include "stdafx.h"
#include <iostream>
#include <vector>
#include <string>

class Generator
{
public:
	Generator(unsigned int len, const std::string& alphabet):
		len(len),alphabet(alphabet)
	{
		variant.resize(len);		
	}
	std::vector<std::string> CreateAllVariants()
	{
		std::vector<std::string> result;

		brute(0, result);

		return result;
	}

private:
	unsigned int len;
	std::string alphabet;
	std::string variant;

	void brute(unsigned int position, std::vector<std::string>& variants)
	{
		for (auto it = alphabet.cbegin(); it != alphabet.cend(); ++it)
		{
			variant[position] = *it;
			if (position + 1 < len)
			{
				brute(position + 1, variants);
			}	

			if (IsValidVariant(variant))
			{
				std::cout << variant << std::endl;
				variants.push_back(variant);
			}
		}	
	}

	bool IsValidVariant(const std::string& value) const
	{
		const unsigned int sum = 0x29A;
		unsigned int total = 0;

		for (auto it = value.cbegin(); it != value.cend(); ++it)
		{
			total += static_cast<unsigned char> (*it);
		}		

		return total == sum;
	}
};

int main()
{
	const std::string alphabet = "abcdefghijklmnopqrstuvwxyz";
	Generator g(6, alphabet);
	for (auto x : g.CreateAllVariants())
	{
		std::cout << x << std::endl;
	}
    return 0;
}

